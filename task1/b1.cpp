#include <iostream>
#include <string>
#include <cmath> // log2
using namespace std;

string strsort(string str) {
    char temp;
    int len = str.length();
    for (int i = 0; i < len; i++){
    
    for (int j = 0; j < len - 1; j++){
        if (str[j] > str[j + 1]){ 
            temp = str[j];
            str[j] = str[j + 1];
            str[j + 1] = temp;
            }
        }
    }
    return str;
}

double shannon_entropy(string str) {
    double probability,
    str_len = str.length(),
    entropy = 0,
    current_counter = 1; 

    str = strsort(str);
    char prev_letter = str[0];
    for (int i = 1; i < str.length()+1; i++) {
        
        if (str[i] == prev_letter) {
            current_counter ++;
            }
        else if (str[i] != prev_letter) {
            probability = current_counter / str_len;
            entropy = entropy - (probability * log2(probability));
    
            prev_letter = str[i];
            current_counter = 1;
            if (i == str.length()-1) {
                probability = (str.length() - i) / str_len;
                entropy = entropy - (probability * log2f(probability));
                }
            }
        }
    return entropy;
}

int main() {
    cout << "Ввод:" << endl;
    string str;
    getline(std::cin, str);

    cout.precision(3);
    cout << "Вывод:" << shannon_entropy(str) << endl;
    
    return 0;
}
