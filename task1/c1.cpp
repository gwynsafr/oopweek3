#include <iostream>
#include <string>
using namespace std;

void syncedtwoarraybubbleSort(int id_array[], double temp_array[], int array_length, int mode=0) {
    // код копировать не красиво, но ставить проверку выбранного вида сортировки в цикл ставить тоже не хочется
    // сортировка по id
    if (mode == 0) {
        int i, j;
        for (i = 0; i < array_length - 1; i++) {
            for (j = 0; j < array_length - i - 1; j++) {
                if (id_array[j] > id_array[j + 1]) {
                    swap(id_array[j], id_array[j + 1]);
                    swap(temp_array[j], temp_array[j + 1]);
                    }
            }
        }
     }
    // сортировка по температуре
    if (mode == 1) {
        int i, j;
        for (i = 0; i < array_length - 1; i++) {
            for (j = 0; j < array_length - i - 1; j++) {
                if (temp_array[j] > temp_array[j + 1]) {
                    swap(id_array[j], id_array[j + 1]);
                    swap(temp_array[j], temp_array[j + 1]);
                    }
            }
        }
     }
}

void structure_inputed_data(string line, int id_array[], double temp_array[], int array_length) {
    int index = -1;
    for (int i = 0; i < line.length(); i++) {
        if (line[i] == '@') {
            index ++;
            id_array[index] = stoi(line.substr(i + 1, 2));
            temp_array[index] = stoi(line.substr(i + 3, 3)) * 1.0;
        }
    }
}

void calculate_average_temperature(int id_array[], double temp_array[], int array_length) {
    syncedtwoarraybubbleSort(id_array, temp_array, array_length, 0);
    int repeated_id_counter = 0;
    double temperature_sum = 0;
    for (int i = 0; i < array_length; i++) {
        if (id_array[i] != 0) {
            while (id_array[i] == id_array[i+1]) {
                repeated_id_counter += 1;
            }
            for (int j = i ; i < i + repeated_id_counter; j++) {
                id_array[j+1] = 0;
                temperature_sum += temp_array[j-1];
            }
            if (repeated_id_counter != 0) {
                temp_array[i] = temperature_sum / repeated_id_counter * 1.00;
                }
            repeated_id_counter = 0;
            temperature_sum = 0;
        }

    }
}

void print_cooked_data(int id_array[], double temp_array[], int data_array_length, int output_style) {
    syncedtwoarraybubbleSort(id_array, temp_array, data_array_length, output_style);
    for (int i = 0; i < data_array_length; i++){
        if (id_array[i] != 0) {
            cout << id_array[i] << " " << temp_array[i] << endl;
        }
    }
}
        
int main() {
    string line;
    getline(cin, line);
    // line = "3744@6646@63-4@461@127";
    line = '@'+line;
    int data_array_length = line.length() / 4 + 1;
    int *id_array = new int[data_array_length];
    double *temp_array = new double[data_array_length];
    int output_style;
    cout << "По возрастанию id - 0" << endl << "По возрастанию средней температуры - 1" << endl;
    cin >> output_style;

    structure_inputed_data(line, id_array, temp_array, data_array_length);
    calculate_average_temperature(id_array, temp_array, data_array_length);
    print_cooked_data(id_array, temp_array, data_array_length, output_style);

    delete [] id_array, temp_array;
}
