#include <iostream>
#include <string>

using namespace std;

int count_substr_in_string_array(string substr, string *str_array, int number_of_strings) {
    // Реализуем наивный алгоритм
    int substr_found_times = 0,
    substr_length = substr.length(),
    str_length = 0;
    for (int i = 0; i < number_of_strings; i++) {
        str_length = str_array[i].length();
        if (str_length > substr_length) {
            for (int j = 0; j <= str_length - substr_length; j++) {
                if (substr == str_array[i].substr(j, substr_length)) {
                    substr_found_times++;
                }
            }
        }
    }
    return substr_found_times;
}

int main() {
    string substr, temp = "";
    int number_of_strings = 3;
    string *str_array = new string[number_of_strings];
    cout << "Что ищем:" << endl;
    cin >> substr;
    cout << substr.substr(0,0);
    cout << "Где ищем: " << endl;
    for (int i = 0; i < number_of_strings; i++) {
        cin >> str_array[i];
    }  

    int substr_found_times = count_substr_in_string_array(substr, str_array, number_of_strings);
    
    cout << "Вывод:" << endl << substr_found_times << endl;
    delete [] str_array;
    return 0;
}
