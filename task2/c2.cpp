#include <regex>
#include <iostream>
#include <string>
using namespace std;

string delete_repeating_chars(string line) {
    return regex_replace (line, regex ("(.)\\1\\1+"), "$1");
}

int main() {
    string line;
    getline(cin, line);
    delete_repeating_chars(line);

    cout << delete_repeating_chars(line) << endl;
    
    return 0;
}
