#include <iostream>
#include <string>
#include <regex>
using namespace std;

bool is_ipv6_address_valid(string address) {
    return regex_match(address, regex ("[[0-9a-f]{4}:[[0-9a-f]{4}:[[0-9a-f]{4}:[[0-9a-f]{4}:[[0-9a-f]{4}:[[0-9a-f]{4}:[[0-9a-f]{4}:[[0-9a-f]{4}"));
}

int main() {
    string address;
    getline(cin, address);
    
    if (is_ipv6_address_valid(address)) {
        cout << "Да" << endl;
    }
    else {
        cout << "Нет" << endl;
    }

}
